#ifndef CONTAINERS_STACK_H
#define CONTAINERS_STACK_H

#include <vector>

namespace containers {

    template<typename T>
    class stack : protected std::vector<T> {
       
    public:

        stack(std::size_t size) : std::vector<T>(size) { };

        T pop() {
            T out = this->back();
            this->pop_back();
            return out;
        }

        void push(const T &val) { std::vector<T>::push_back(val); }

        void push(T &&val) { std::vector<T>::push_back(val); }

    };

}
#endif
