#ifndef RPN_CALCULATOR_RPN_H
#define RPN_CALCULATOR_RPN_H

#include <iostream>
#include <unordered_map>
#include <limits>
#include <cmath>
#include <vector>


namespace rpn {

    enum token_type {
        OPERATOR,
        NUMBER
    };

    enum operator_type {
        ADD,
        SUBTRACT,
        MULTIPLY,
        DIVIDE,
    };

    struct token {

        token_type type;
        operator_type op;
        long double number;

        token(const std::string &str);

        token() { };

        token(const long double &num) : type(NUMBER), number(num) { };

    };
    long double parse_rpn(const std::string &line);
};

#endif
