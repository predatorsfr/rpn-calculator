#include <ios>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <unordered_map>
#include <cstring>
#include <random>
#include <iomanip>
#include "./includes/rpn_calc.hpp"

using namespace rpn;
using namespace std;

int main() {
    using std::cerr;
    using std::cout;
    using std::endl;
    using std::cin;

    string line_c_str;

    while (true) {

        cout << "RPN Calculator >";
        getline(cin, line_c_str);
        
        if (line_c_str == "quit") {
            exit(0);
        } else if (line_c_str == "help") {
            cout << "The “clear” command will reset the calculator.\nThe “quit” command will exit the calculator.\nThe “help” command will display online help from the list of available commands.\n";
        } else if ( line_c_str == "clear") {
            system("clear");
        } else {
            try {
                    cout << parse_rpn(line_c_str) << endl;
            } catch (runtime_error &e) {
                    cerr << "Error: " << e.what() << endl;
            }
        }
    }
}