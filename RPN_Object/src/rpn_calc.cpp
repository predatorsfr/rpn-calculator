#include <cmath>
#include <string>
#include <sstream>
#include <vector>
#include "../includes/stack.hpp"
#include "../includes/rpn_calc.hpp"

using namespace std;

namespace rpn {

    using std::cout;
    using std::endl;
    using std::cerr;

    token::token(const string &str) {

        if ((str.front() >= '0' && str.front() <= '9') || str.front() == '.') {
            type = NUMBER;
            number = stold(str);
        } else {
            type = OPERATOR;
            if (str == "+") {
                op = ADD;
            } else if (str[0] == '-') {
                if (isdigit(str[1])) {
                    type = NUMBER;
                    number = stold(str);
                }
                op = SUBTRACT;
            } else if (str == "*") {
                op = MULTIPLY;
            } else if (str == "/") {
                op = DIVIDE;
            } else {
                throw runtime_error("Invalid Token: " + str);
            }
        }
    }


    long double parse_rpn(const string &line) {

        containers::stack<token> stack(50);
        string word;
        stringstream stringstream(line);
        
        while (stringstream >> word) {
            token t(word);
            if (t.type == OPERATOR) {
                switch (t.op) {
                    case ADD: {
                        long double ex1(stack.pop().number);
                        long double ex2(stack.pop().number);
                        stack.push(token(ex1 + ex2));
                        break;
                    }
                    case SUBTRACT: {
                        long double ex1(stack.pop().number);
                        long double ex2(stack.pop().number);
                        stack.push(token(ex2 - ex1));
                        break;
                    }
                    case MULTIPLY: {
                        long double ex1(stack.pop().number);
                        long double ex2(stack.pop().number);
                        stack.push(token(ex1 * ex2));
                        break;
                    }
                    case DIVIDE: {
                        long double ex1(stack.pop().number);
                        long double ex2(stack.pop().number);
                        stack.push(token(ex2 / ex1));
                        break;
                    }
                    default:
                        cerr << endl;
                        cerr << word << endl;
                        cerr << t.type << endl;
                        cerr << t.number << endl;
                        cerr << t.op << endl;
                        throw runtime_error("Invalid Token");
                }
            } else {
                stack.push(t);
            }
        }
        return stack.pop().number;
    }
}