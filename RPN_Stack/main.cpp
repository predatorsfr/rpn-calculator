#include <iostream>
#include <fstream>
#include <sstream>
#include <stack>
#include <string>

using namespace std;

string getInput();
string popOperand(stack<string>& m);

int main()
{
    string uI;
    string part;
    string infix = "";
    stack<string> memory;
    char prevOperator = ' ';
    double b = 0;

while(true) {
    uI = getInput();
    istringstream iss(uI);

    while (iss >> part) {

        switch(part[0]) {

            case '+':
                infix = infix + " + " + memory.top();
                memory.push(to_string(stod(popOperand(memory)) + stod(popOperand(memory))));
                prevOperator = '+';
                break;

            case '-':
                if (isdigit(part[1])) {
                    memory.push(part);
                    break;
                }
                infix = infix + " - " + memory.top();
                b = stod(popOperand(memory));
                memory.push(to_string(stod(popOperand(memory)) - b));
                prevOperator = '-';
                break;

            case '*':
                if (prevOperator == '+' || prevOperator == '-')
                    infix = "(" + infix + ") * " + memory.top();
                else
                    infix = infix + " * " + memory.top();
                memory.push(to_string(stod(popOperand(memory)) * stod(popOperand(memory))));
                prevOperator = '*';
                break;

            case '/':
                if (prevOperator == '+' || prevOperator == '-')
                    infix = "(" + infix + ") / " + memory.top();
                else
                    infix = infix + " / " + memory.top();
                b = stod(popOperand(memory));
                if (b == 0)
                    cout << "ERROR: Division by zero." << endl;
                memory.push(to_string(stod(popOperand(memory)) / b));
                prevOperator = '/';
                break;

            default:
                if (isdigit(part[0])) {
                    memory.push(part);
                    if (infix == "")
                        infix = part;
                }
                break;

        } 
    }
    cout << "The result of " << infix << " is: " << stod(popOperand(memory)) << endl;
}
return 0;
}

string getInput()
{
    string inputMethod;

    while (true) {
        cout << "RPN Calculator >";
        getline(cin, inputMethod);
        if ((inputMethod.length() < 4 && (inputMethod != "quit" || inputMethod != "help" || inputMethod != "clear")))
            cout << "Invalid input! Please, try again.\n";
        else
            break;
    }

    switch(inputMethod[0]) {

        case 'q':
        {
           exit(0);
        }
            break;

        case 'h' :
        {
            cout << "The “clear” command will reset the calculator.\nThe “quit” command will exit the calculator.\nThe “help” command will display online help from the list of available commands.\n";
            getInput();
        }
            break;

        case 'c':
        {
            system("clear");
            getInput();
        }
            break;

        default:
            break;
    }
    return inputMethod;
}

string popOperand(stack<string>& m)
{
    string value;
    if (!m.empty()) {
        value = m.top();
        m.pop();
        return value;
    } else
        cout << "ERROR: Trying to read from empty stack!" << endl;
    return 0;
}